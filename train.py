"""
TRAIN GANOMALY

Example: Run the following command from the terminal.
    run train.py                             \
        --gpu_ids -1 --model ganomaly                        \
        --dataset mnist \
        --batchsize 32                          \
        --isize 256                         \
        --nz 512                                \
        --ngf 64                               \
        --ndf 64
"""

# Files already downloaded and verified
# Files already downloaded and verified
# >> Training model Ganomaly.
# 100% 703/703 [20:16<00:00,  1.73s/it]
# >> Training model Ganomaly. Epoch 1/15
#    Avg Run Time (ms/batch): 274.149 AUC: 0.621 max AUC: 0.621
# >> Training model Ganomaly. Epoch 2/15
#    Avg Run Time (ms/batch): 284.825 AUC: 0.649 max AUC: 0.649
#   2% 11/703 [00:18<20:00,  1.73s/it]Process Process-34:
# Process Process-35:
# Process Process-33:



# LIBRARIES
from __future__ import print_function

from options import Options
from lib.data import load_data

from lib.model import Ganomaly

# def main():
""" Training"""

# ARGUMENTS
opt = Options().parse()

opt.niter = 5

opt.ngpu = 0
#opt.display = True

print(opt)



# LOAD DATA
dataloader = load_data(opt)

# LOAD MODEL
model = Ganomaly(opt, dataloader)

# TRAIN MODEL
model.train()

# if __name__ == '__main__':
#     main()

# Namespace(anomaly_class='car', batchsize=64, beta1=0.5, dataroot='', dataset='cifar10', device='cpu',
# display=False, display_id=0, display_port=8097, display_server='http://localhost', droplast=True, extralayers=0,
# gpu_ids=[0], isTrain=True, isize=32, iter=0, load_weights=False, lr=0.0002, manualseed=-1, metric='roc',
# model='ganomaly', name='ganomaly/cifar10', nc=3, ndf=64, ngf=64, ngpu=0, niter=5, nz=100, outf='./output',
# phase='train', print_freq=100, proportion=0.1, resume='', save_image_freq=100, save_test_images=False,
# w_bce=1, w_enc=1, w_rec=50, workers=8)

# Files already downloaded and verified
# Files already downloaded and verified
#   0%|          | 0/703 [00:00<?, ?it/s]
# >> Training model Ganomaly.
# >> Training model Ganomaly. Epoch 1/5
#   0% 0/703 [00:00<?, ?it/s]   Avg Run Time (ms/batch): 278.323 AUC: 0.627 max AUC: 0.627
# >> Training model Ganomaly. Epoch 2/5
#   0% 1/703 [00:05<1:06:11,  5.66s/it]   Avg Run Time (ms/batch): 326.286 AUC: 0.602 max AUC: 0.627
# >> Training model Ganomaly. Epoch 3/5
#    Avg Run Time (ms/batch): 302.748 AUC: 0.608 max AUC: 0.627
# >> Training model Ganomaly. Epoch 4/5
#   1% 6/703 [00:09<18:18,  1.58s/it]   Avg Run Time (ms/batch): 258.091 AUC: 0.669 max AUC: 0.669
#  45% 317/703 [11:51<14:51,  2.31s/it]
# Process Process-68:
# Process Process-65:

# Files already downloaded and verified
# Files already downloaded and verified
# >> Training model Ganomaly.
# 100% 703/703 [20:16<00:00,  1.73s/it]
# >> Training model Ganomaly. Epoch 1/15
#    Avg Run Time (ms/batch): 274.149 AUC: 0.621 max AUC: 0.621
# >> Training model Ganomaly. Epoch 2/15
#    Avg Run Time (ms/batch): 284.825 AUC: 0.649 max AUC: 0.649
#   2% 11/703 [00:18<20:00,  1.73s/it]Process Process-34:
# Process Process-35:

