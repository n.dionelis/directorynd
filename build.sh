#!/bin/sh

set e


cd `dirname $0`
rm -rf gym_seasonals.egg-info
python setup.py sdist
fullname=`python setup.py --fullname`
tarfile=dist/${fullname}.tar.gz

echo = Build source distribution \(sdist\)
python setup.py sdist

echo = Build images

echo === Build remote-env
docker build -t remote-env -f docker/remote-env --build-arg source=$tarfile . || exit $?

echo === Build base environments
docker build -t trading-agent:anaconda3 -f docker/trading-agent \
       --build-arg source=$tarfile \
       --build-arg BASE=continuumio/anaconda3 \
       --build-arg TAG=latest . || exit $?
docker build -t trading-agent:tensorflow -f docker/trading-agent \
       --build-arg source=$tarfile \
       --build-arg BASE=tensorflow/tensorflow \
       --build-arg TAG=latest-py3         . || exit $?

echo === Build example agents
docker build -t random-agent -f docker/random-agent . || exit $?
docker build -t dqn-agent -f docker/dqn-agent --build-arg TAG=tensorflow . || exit $?